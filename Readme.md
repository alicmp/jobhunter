# Job Hunter
With this project you can find job posts on reddit and craigslist based on
your speciality. Its on early stages but its going to extract posts on reddit
and craigslist and with a help of spaCy library its deciding whether this post
is good match with your specialities or not.
Right now i wrote my own filter method for this task and its working fine, but
later i implement spaCy with it.

## Requirements
- Python 3
- Pipenv

## Setup
Install dependencies then execute `main.py`

## Future development
- [] Write Craigslist importer as well.
- [] Create webapp and let other users enter their email and their tech and send 
related job posts to their email.
- [] Use spaCy for filtering relatable job posts.